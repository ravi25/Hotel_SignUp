
exports.reportTemplate = function(hotel_data){
	var text = '';
	text =  text + '<!DOCTYPE html><html lang="en"><head><title>Bootstrap Example</title>'
					+'<meta charset="utf-8">'
					+'<meta name="viewport" content="width=device-width, initial-scale=1">'
					+'<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">'
					+'<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>'
					+'<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>'
					+'</head><body><div class="container">'

	if(hotel_data['type']=='H')
        text = text + "<h2>Hourly Report</h2>";
    else{
    	text = text + "<h2>Today Report</h2>";
    }
    text = text  + '<table class="table table-hover" border="1">'
			+'<thead><tr>'
			   +'<th colspan="2">Hotel Email ID</th>'
			   +'<th colspan="2">Hotel Name</th>'
			   +'<th colspan="2">Hotel Authorized Signatory Name</th>'
			   +'<th colspan="2">Hotel Address</th>'
			   +'<th colspan="2">Commission %</th>'
			   +'<th colspan="2">BD Person Name</th>'
			   +'<th colspan="2">BD Person Phone number</th>'
			   +'<th colspan="2">PDF Link</th>'
			+'</thead></tr><tbody>'
	for(var i=0;i<hotel_data['data'].length ; i++){
		text = text + '<tr>'
						   +'<td colspan="2">' + hotel_data['data'][i]['email_id'] +'</td>'
						   +'<td colspan="2">' + hotel_data['data'][i]['name'] +'</td>'
						   +'<td colspan="2">' + hotel_data['data'][i]['authorized_signatory_name'] +'</td>'
						   +'<td colspan="2">' + hotel_data['data'][i]['address'] +'</td>'
						   +'<td colspan="2">' + hotel_data['data'][i]['commission'] +'</td>'
						   +'<td colspan="2">' + hotel_data['data'][i]['bd_person_name'] +'</td>'
						   +'<td colspan="2">' + hotel_data['data'][i]['bd_mobile_number'] +'</td>'
						   +'<td colspan="2">' + hotel_data['data'][i]['pdf_url'] +'</td>'
						+'</tr>'
	}
	text = text + "</tbody></table></body></html>";
	return text;
}