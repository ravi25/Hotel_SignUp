var express = require('express');
var router = express.Router();
var otp_sender = require('../lib/otp_sender.js');
var user = require('../lib/user.js');
var validator = require('../lib/validation.js');
router.post('/generateagreement', user.fetchAgreementForm);
router.post('/sendPdfMail',otp_sender.generatePdfFile);
router.get('/validateURL',validator.validateURL);
module.exports = router;
