var mongoose = require('mongoose'); 
mongoose.connect('mongodb://localhost/nodewebappdb');
var hotel_details = new mongoose.Schema({
	email_id: String,
	name: String,
	authorized_signatory_name: String,
	address: String,
	commission: Number,
	mobile_number : Number,
	bd_person_name : String,
	bd_mobile_number : Number,
	created_on : String,
	created_by : String,
	accepted : Boolean,
	pdf_url : String,
	session_id : String
})
var Hotel_Detail = mongoose.model('hotel_details',hotel_details);
module.exports = Hotel_Detail;