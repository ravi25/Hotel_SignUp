var s3 = require('s3');
    fs = require('fs');
var http = require('http');
var AWS = require('aws-sdk');
var hotel_details = require('../model/otp_details.js');
AWS.config.loadFromPath('/home/ubuntu/nodewebapp/s3_upload/s3_config.json');
var config = require('../helper/config');
exports.uploadPdfFile = function(hotel_name,session_id){
      console.log('uploading started...');
      var date = new Date();
      date = date.getHours()  + '-' + date.getMinutes() + '-' + date.getSeconds() + '-' + date.getDay() + '-' + date.getMonth()  + '-' + date.getYear();
      var client = s3.createClient(config.s3_configuration);
	    var params = {
          localFile: config.pdf_file_path,
          s3Params: {
              Bucket: "paytm-agreements",
              Key: "hotels/" + date + "__" + hotel_name + ".pdf",
          },
      };
      var uploader = client.uploadFile(params);
      console.log(params);
      uploader.on('error', function(err,data) {
          console.error("unable to upload:", err.stack);
          return 0;
      });
      uploader.on('progress', function() {
          console.log("progress", uploader.progressMd5Amount,
              uploader.progressAmount, uploader.progressTotal);
      });
      uploader.on('end', function() {
          console.log("done uploading");
          var s3Bucket = new AWS.S3( { params: {Bucket: 'paytm-agreements'} } );
          var urlParams = {Bucket: 'paytm-agreements', Key: params.s3Params.Key};
          console.log(urlParams);
              s3Bucket.getSignedUrl('getObject', urlParams, function(err, url){
                if(err){
                  console.log('s3 serevr error',err);
                }
                else{
                  console.log('the url of the pdf file is is', url);
                  console.log(url);
                  var b = url;
                  console.log(b);
                  console.log(session_id);
                  var query = { 
                      "session_id" : session_id
                  };
                  hotel_details.update(query, {"pdf_url" : b},function(err) {
                        if( err) {
                          console.log("no data present in collection");
                        }
                        else {
                          console.log('succesfully updated');
                        }
                      }); 
                }
              })
      });
}

/*s3.getPublicUrl(params.s3Params.Bucket,params.s3Params.Key,config.s3_bucket_location,function(error,url){
            if(error){
              console.log('error is happening in s3 server');
              return 0;
            }
            else{
              console.log('succesfully getting url',url);
              return url;
            }
          })*/
