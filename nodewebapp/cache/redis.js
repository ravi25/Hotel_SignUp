var Redis = require('ioredis');
var uuid = require('node-uuid');
var config = require('../helper/config');
var slave_redis = new Redis(config.cache.redis.slaves[0].port, config.cache.redis.slaves[0].host);
console.log('port of slave db is:',config.cache.redis.slaves[0].port);
exports.saveHotelDeatilsInRedis = function(sessionId,hotel_detail){
	console.log('coming to Redis');
	console.log(hotel_detail);
	var request_expire = 24 * 60 * 60*1000;
	var data = {};
	var key = sessionId;
	data['url'] = "http://localhost:3000/validateUrl/?sessionid=" + key;
	console.log('url is',data['url']);
	data['hotel_detail'] = hotel_detail;
	slave_redis.set(key,JSON.stringify(data));
	slave_redis.expire(key, request_expire);
	var context = {};
	context['sessionId'] = key;
	context['url'] = data['url'];
	return context;
}

exports.setDataInCache = function(key,data){
    slave_redis.set(key,JSON.stringify(data));
}

exports.expireKeyFromCache = function(key,expire_time){
	slave_redis.expire(key, expire_time);
}

exports.checkKeyisPresentinCache = function(key,cb){
	var temp = slave_redis.exists(key,function(err,data){
         return cb(err,data);
	});
}

exports.removeKeyFromCache = function(key){
	slave_redis.del(key);
}

exports.getValueFromKeyInCache = function (key,cb){
	var temp = slave_redis.get(key,function(err,data){
		//console.log(err,data,"data");
		return cb(err,data);
	});
}

exports.generateSessionID = function(email_id,sessionid){
	return email_id + sessionid;
}